from tkinter import *
from tkinter import messagebox, ttk
import sqlite3
import datetime

# Create Connection(Finish)
def createconnection() :
    global conn,cursor
    conn = sqlite3.connect('CnC database.db')
    cursor = conn.cursor()

# Check Input(Finish)
def check_input_type(input):
    try:
        # Convert it into integer
        int(input)
        return('int')
    except ValueError:
        try:
            # Convert it into float
            float(input)
            return('float')
        except ValueError:
            return('str')

# Create Window(Finish)
def createWindow():
    window = Tk()
    import_Img()
    window.title('Owner interface')
    window.resizable(False, False)
    window.geometry('900x600+300+150')
    window.config(bg='#735000')
    window.iconphoto(False,img1)
    return window

# Main Window
def mainWindow():
    f1=Frame(window, height=1000, width=1000, bg='#735000').place(x=0,y=0)
    Label(window, image=img1, bg='#735000').place(x=350,y=10)
    Label(f1,text='Cake n(&) Coffee Cafe',font='Handwriting 30 bold',fg='white',bg='#735000').place(x=240,y=230)
    LG = Button(f1,text='Login',height=3,width=13,font='Tohoma 10 bold',command=loginWindow)
    LG.place(x=400,y=300)

# Login Window
def loginWindow():
    f1=Frame(window, height=800, width=800, bg='#735000').place(x=0,y=0)
    Label(f1,text='Login Page',font='Tohoma 18 bold',fg='white',bg='#735000').place(x=400,y=10)
    Label(f1,text='User Name ',font='Tohoma 12 bold',fg='white',bg='#735000').place(x=430,y=100)
    Label(f1,text='Password ',font='Tohoma 12 bold',fg='white',bg='#735000').place(x=430,y=200)
    user = Entry(f1,font='Tohoma 14 bold',width=15,textvariable=puser)
    password = Entry(f1,font='Tohoma 14 bold',width=15,textvariable=ppassword,show='*')
    lg = Button(f1,text='Login',height=3,width=13,font='Tohoma 10 bold',command=ownerMenu)
    user.place(x=400,y=150)
    password.place(x=400,y=250)
    lg.place(x=400,y=300)
    customerButton = Button(f1,text='For Customer',height=2,width=14,font='Tohoma 10 bold',command=customerWindow)
    customerButton.place(x=760,y=520)
    back = Button(f1,text='Back',height=3,width=13,font='Tohoma 10 bold',command=mainWindow)
    back.place(x=10,y=500)

# Owner Menu (View Menu Page is not Finish)
def ownerMenu():
    f1=Frame(window, height=1000, width=1000, bg='#735000').place(x=0,y=0)
    Label(f1,text='Funtion',font='Tohoma 18 bold',fg='white',bg='#735000').place(x=400,y=10)

    Emlists = Button(f1,text='ดูรายชื่อพนักงาน',height=3,width=13,font='Tohoma 10 bold',command=emlistWindow)
    Emlists.place(x=380,y=100)

    Incomelists = Button(f1,text='ดูรายรับ',height=3,width=13,font='Tohoma 10 bold',command=Incomelist)
    Incomelists.place(x=380,y=200)

    stocks = Button(f1,text='สต๊อก',height=3,width=13,font='Tohoma 10 bold',command=stock)
    stocks.place(x=380,y=300)

    function = Button(f1,text='Menulist',height=3,width=13,font='Tohoma 10 bold',command=view_menu)
    function.place(x=380,y=400)
    
    viewOrder = Button(f1,text='View Order',height=3,width=13,font='Tohoma 10 bold',command=viewOrder_Window)
    viewOrder.place(x=550,y=100)

    back = Button(f1,text='Back',height=3,width=13,font='Tohoma 10 bold',command=loginWindow)
    back.place(x=10,y=500)

#ดูรายชื่อพนักงาน(Interface(Treeview, Window))
def emlistWindow():
    # Treeview Interface
    def treeviewinfo():
        # Create Info Treeframe Frame
        treeframe_info = Frame(info_frame, height=800, width=800)
        treeframe_info .place(x=60,y=100)

        # Create Scrollbar
        treebar = Scrollbar(treeframe_info)
        treebar.pack(side=RIGHT,fill=Y)
        mytree_info = ttk.Treeview(treeframe_info,columns=("Fname","Lname","tel","position","salary"),height=10,yscrollcommand=treebar.set)
        mytree_info.pack()

        # Config scrollbar on the treeview
        treebar.config(command=mytree_info.yview)

        # Rename Heading
        mytree_info.heading("#0",text="",anchor=W)
        mytree_info.heading("Fname",text="First name",anchor=CENTER)
        mytree_info.heading("Lname",text="Last Name",anchor=CENTER)
        mytree_info.heading("tel",text="Tel",anchor=CENTER)
        mytree_info.heading("position",text="Position",anchor=CENTER)
        mytree_info.heading("salary",text="Salary",anchor=CENTER)
    
        # Adjust Column Size
        mytree_info.column("#0",width=0,minwidth=0)
        mytree_info.column("Fname",anchor=W,width=120,minwidth=105)
        mytree_info.column("Lname",anchor=W,width=120,minwidth=105)
        mytree_info.column("tel",anchor=W,width=120,minwidth=105)
        mytree_info.column("position",anchor=W,width=120,minwidth=105)
        mytree_info.column("salary",anchor=W,width=120,minwidth=105)
        mytree_info.delete(*mytree_info.get_children())
        
        # Reset Treeview
        mytree_info.delete(*mytree_info.get_children())

        # Import Data From Database
        sql = 'select * from Employee'
        cursor.execute(sql)
        result = cursor.fetchall()

        # Insert Data From Database into list
        for i,data in enumerate(result):
            idinfo_lst.append(data[0])
            fnameinfo_lst.append(data[1])
            lnameinfo_lst.append(data[2])
            telinfo_lst.append(data[3])
            positioninfo_lst.append(data[4])
            salaryinfo_lst.append(data[5])

        # Insert Data from list into Treeview
        for i, data in enumerate(idinfo_lst):
            mytree_info.insert('', 'end', values=(fnameinfo_lst[i], lnameinfo_lst[i], telinfo_lst[i], positioninfo_lst[i], salaryinfo_lst[i]))

        return mytree_info

    def searchinfo():
        # Reset Medical Stock Display
        mytree_info.delete(*mytree_info.get_children())

        # Insert Treeview Data
        for i, data in enumerate(fnameinfo_lst):
            if (search.get()).lower() in (fnameinfo_lst[i]).lower() or (search.get()).lower() in (lnameinfo_lst[i]).lower():
                mytree_info.insert('', 'end', values=(fnameinfo_lst[i], lnameinfo_lst[i], telinfo_lst[i], positioninfo_lst[i], salaryinfo_lst[i]))

    # Variable
    idinfo_lst = []
    fnameinfo_lst = []
    lnameinfo_lst = []
    telinfo_lst = []
    positioninfo_lst = []
    salaryinfo_lst = []

    # Create Doctor Window Frame
    doctor_frame=Frame(window, height=800, width=800, bg='#735000')
    doctor_frame.place(x=0,y=0)

    # Create Info Frame
    info_frame = Frame(window, height=800, width=800, bg='#735000')
    info_frame.place(x=10,y=0)

    # Create Label
    Label(info_frame,bg="#50988d").place(x=386,y=39,relheight=0.077,relwidth=0.076)

    # Create Button
    Button(info_frame,image=imgsearch,bg="#83c7bd",activebackground = "#abd4ce",bd=0.38,command=searchinfo).place(x=388,y=41,relheight=0.068,relwidth=0.07)
    back = Button(info_frame,text='Back',width=15,height=2,font='Tohoma 10 bold',command=ownerMenu)
    back.place(x=10,y=500)
    # Create Entry
    search = Entry(info_frame,bg="#d7fcfd",font="Tahoma 17")
    search.place(x=60,y=40,relheight=0.07,relwidth=0.4)

    # Call Treeview Info To Display Treeview
    mytree_info = treeviewinfo()

#ดูรายรับ(NOT FINISH Interface)
def Incomelist():
    def treeviewinfo():
        # Create Info Treeframe Frame
        treeframe_info = Frame(info_frame, height=800, width=800)
        treeframe_info .place(x=60,y=100)

        # Create Scrollbar
        treebar = Scrollbar(treeframe_info)
        treebar.pack(side=RIGHT,fill=Y)
        mytree_info = ttk.Treeview(treeframe_info,columns=("#1","#2","#3"),height=10,yscrollcommand=treebar.set)
        mytree_info.pack()

        # Config scrollbar on the treeview
        treebar.config(command=mytree_info.yview)

        # Rename Heading
        mytree_info.heading("#0",text="",anchor=W)
        mytree_info.heading("#1",text="Table Name",anchor=CENTER)
        mytree_info.heading("#2",text="Balance",anchor=CENTER)
        mytree_info.heading("#3",text="Day",anchor=CENTER)

        # Adjust Column Size
        mytree_info.column("#0",width=0,minwidth=0)
        mytree_info.column("#1",anchor=W,width=120,minwidth=105)
        mytree_info.column("#2",anchor=W,width=120,minwidth=105)
        mytree_info.column("#3",anchor=W,width=120,minwidth=105)
        
        # Reset Treeview
        mytree_info.delete(*mytree_info.get_children())

        # Import Data From Database
        sql = 'select * from Income'
        cursor.execute(sql)
        result = cursor.fetchall()

        # Insert Data From Database into list
        for i,data in enumerate(result):
            tableName_lst.append(data[0])
            balance_lst.append(data[1])
            day_lst.append(data[2])


        # Insert Data from list into Treeview
        for i, data in enumerate(tableName_lst):
            mytree_info.insert('', 'end', values=(tableName_lst[i], balance_lst[i], day_lst[i]))

        return mytree_info

    def searchinfo():
        # Reset Medical Stock Display
        mytree_info.delete(*mytree_info.get_children())

        # Insert Treeview Data
        for i, data in enumerate(tableName_lst):
            if (tableNum.get()) == (tableName_lst[i]):
                mytree_info.insert('', 'end', values=(tableName_lst[i], balance_lst[i], day_lst[i]))

    # Variable
    tableName_lst = []
    balance_lst = []
    day_lst = []
    tableNum = StringVar()

    # Create Doctor Window Frame
    ncome_frame=Frame(window, height=800, width=800, bg='#735000')
    ncome_frame.place(x=0,y=0)

    # Create Info Frame
    info_frame = Frame(window, height=800, width=800, bg='#735000')
    info_frame.place(x=10,y=0)

    # Create Label
    Label(info_frame,bg="#50988d").place(x=386,y=39,relheight=0.077,relwidth=0.076)

    # Create Button
    Button(info_frame,image=imgsearch,bg="#83c7bd",activebackground = "#abd4ce",bd=0.38, command=searchinfo).place(x = 650 ,y = 12)

    # Create Entry
    tablelist = ["Table 1","Table 2","Table 3","Table 4","Table 5","Table 6","Table 7"]
    tablebox = ttk.Combobox(info_frame,textvariable=tableNum,state='readonly')
    tablebox['values'] = tablelist
    tablebox.set("Table 1")
    tablebox.place(x=500,y=18)
    # Call Treeview Info To Display Treeview
    mytree_info = treeviewinfo()

# Check Stock(Interface(Treeview, Window))
def stock():
    # Stock Window Function
    def add_data():
        # Access Database   
        cursor.execute("SELECT * FROM Stock")
        result = cursor.fetchall()

        # Add data from Database to list
        for i,data in enumerate(result):
            IDstocklst.append(data[0])
            namestock_lst.append(data[1])
            amtstock_lst.append(float(data[2]))
            unitstock_lst.append(data[3])

    # Treeview Interface
    def treeview():
        # Variable
        nonlocal add_data
        # Create Treeview Frame
        treeframe_stock = Frame(stock_frame)
        treeframe_stock .place(x=40,y=60)

        # Create Scrollbar
        treebar = Scrollbar(treeframe_stock)
        treebar.pack(side=RIGHT,fill=Y)
        mytree_stock = ttk.Treeview(treeframe_stock,columns=("name","amount","unit"),height=10,yscrollcommand=treebar.set)
        mytree_stock.pack()

        # Config scrollbar on the treeview
        treebar.config(command=mytree_stock.yview)

        # Rename Headings
        mytree_stock.heading("#0",text="",anchor=W)
        mytree_stock.heading("name",text="Name",anchor=CENTER)
        mytree_stock.heading("amount",text="Amount",anchor=CENTER)
        mytree_stock.heading("unit",text="Unit",anchor=CENTER)
    
        # AdjustColumn Size
        mytree_stock.column("#0",width=0,minwidth=0)
        mytree_stock.column("name",anchor=W,width=300,minwidth=300)
        mytree_stock.column("amount",anchor=E,width=100,minwidth=100)
        mytree_stock.column("unit",anchor=E,width=100,minwidth=100)

        # Reset Treeview
        mytree_stock.delete(*mytree_stock.get_children()) 

        add_data()

        # Add data form list to Treeview
            
        for i, data in enumerate(namestock_lst):
            mytree_stock.insert('', 'end', values=(namestock_lst[i], amtstock_lst[i], unitstock_lst[i]))
        
        # Event Bind Treeview
        mytree_stock.bind('<Double-1>',treeviewstockclick)

        return mytree_stock
    
    def treeviewstockclick(event) :
        nonlocal origin_name
        # Delete Exist Information in Entry
        namemed.delete(0,END)
        amount.delete(0,END)
        unit.delete(0,END)

        values = mytree_stock.item(mytree_stock.focus(),'values')

        origin_name = values[0]
        # Insert Select Information into Entry
        namemed.insert(0,values[0])   
        amount.insert(0,values[1])
        unit.insert(0,values[2])

    def search():
        # Reset Medical Stock Display
        mytree_stock.delete(*mytree_stock.get_children())

        # Reinsert Medical Stock Display
        for i, data in enumerate(namestock_lst):
            if (serch_stock.get().lower()) in (namestock_lst[i]).lower():
                mytree_stock.insert('', 'end', values=(namestock_lst[i], amtstock_lst[i], unitstock_lst[i]))

    def add() :
        correction = check_input(namemed.get(),amount.get(),unit.get(),'added')

        if correction == True:
            
            if namemed.get() in namestock_lst:
                msg = messagebox.askquestion('Unsuccessfully Added','This menu is already existed, do you want to update old info?')
                if msg == 'yes':
                    update()
            else:       
                mytree_stock.insert('',index='end',values=(namemed.get(),amount.get(),unit.get()))
                cursor.execute("INSERT INTO Stock (name,amount,unit VALUES (?,?,?)",(namemed.get(),amount.get(),unit.get()))
                conn.commit()
                messagebox.showinfo("Succesfully Added","Succesfully Added new info to Stock")
                # Clear Existed list Data
                IDstocklst.clear()
                namestock_lst.clear()
                amtstock_lst.clear()
                unitstock_lst.clear()

                # Add data to list
                add_data()
                # Clear Every Entry on Stock Page
                clear()

    def update() :
        correction = check_input(namemed.get(),amount.get(),unit.get(),'updated')
        
        if correction == True:
            selected = mytree_stock.focus()
            mytree_stock.item(selected,text="",values=(namemed.get(),amount.get(),unit.get(), IDstocklst[namestock_lst.index(origin_name)]))

            sql = '''
                    update Stock
                    set  name=?, amount=? , unit=?
                    where id=?
            '''

            cursor.execute(sql,[namemed.get(),amount.get(),unit.get(), IDstocklst[namestock_lst.index(origin_name)]])
            conn.commit()

            messagebox.showinfo("Admin:","Update succesfully")
            namemed.delete(0,END)
            amount.delete(0,END)
            unit.delete(0,END)

    def remove() :
        deleterow = mytree_stock.selection()
        values = mytree_stock.item(mytree_stock.focus(),'values')
        mytree_stock.delete(deleterow)
        sql = "delete from Stock where id=?"
        cursor.execute(sql,[values[0]])
        conn.commit()
        messagebox.showinfo("Admin:","Delete succesfully")
        clear()

    def clear():
        serch_stock.delete(0,END)
        namemed.delete(0,END)
        amount.delete(0,END)
        unit.delete(0,END)

    def check_input(name, price, amt, str_type):
        # Check any if empty entry
        if len(name) == 0:
            messagebox.showwarning("Unsuccessfully"+str_type,"Please Enter Product Name")
            return False
        elif len(price) == 0:
            messagebox.showwarning("Unsuccessfully"+str_type,"Please Enter Product Amount")
            return False
        elif len(amt) == 0:
            messagebox.showwarning("Unsuccessfully"+str_type,"Please Enter Product Unit")
            return False
        else:
            
        # Check any wrong input
            try:
                # Check if amount is integer
                amt = float(amt)
                try:
                    # Check if amount is float
                    amt = int(amt)
                    if amt < 0:
                        messagebox.showwarning('Unsuccessfully'+str_type,'Amount Should not be negative.')
                        return False
                    else:
                        return True
                # Amount is not integer Error Message Popup
                except ValueError:
                    messagebox.showwarning('Unsuccessfully'+str_type,'Amount Should be a Whole Number.')
                    return False
            except ValueError:
                messagebox.showwarning('Unsuccessfully'+str_type,'Amount Should be Numeric.')
                return False
                

    # Variable
    IDstocklst = []
    namestock_lst = []
    amtstock_lst = []
    unitstock_lst = []
    origin_name = None

    # Create Doctor Window Frame
    doctor_frame=Frame(window, height=800, width=800, bg='#735000')
    doctor_frame.place(x=0,y=0)

    # Create Stock Frame
    stock_frame=Frame(doctor_frame,height=800,width=600,bg="#9bccc4")
    stock_frame.place(x=205,y=0)

    # Create Stock Frame Label
    Label(stock_frame,bg="#50988d").place(x=290,y=18,relheight=0.077,relwidth=0.076)
    Button(stock_frame,image=imgsearch,bg="#83c7bd",activebackground = "#abd4ce",bd=0.38,command=search).place(x=292,y=20,relheight=0.068,relwidth=0.07)
    serch_stock = Entry(stock_frame,bg="#d7fcfd",font="Tahoma 17")
    serch_stock.place(x=40,y=20,relheight=0.07,relwidth=0.4)

    # Create Stock Frame Button
    Button(stock_frame,text="Add ",width=15,height=2,command=add).place(x=35,y=400)
    Button(stock_frame,text="Update",width=15,height=2,command=update).place(x=175,y=400)
    Button(stock_frame,text="Delete",width=15,height=2,command=remove).place(x=315,y=400)
    Button(stock_frame,text="Clear",width=15,height=2,command=clear).place(x=455,y=400)
    back = Button(stock_frame,text='Back',width=15,height=2,font='Tohoma 10 bold',command=ownerMenu)
    back.place(x=10,y=500)
    # Create Entry Frame
    entry_frame = Frame(stock_frame,bg="#d7fcfd")
    entry_frame.place(x=40,y=300)

    # Create Entry Frame Label
    ccode= Label(entry_frame,text="Name",width=20,height=2,bg='DodgerBlue')
    ccode.grid(row=0,column=0,sticky='news')
    cname = Label(entry_frame,text="amount",height=2,bg='DodgerBlue')
    cname.grid(row=0,column=1,sticky='news')
    cd = Label(entry_frame,text="unit",height=2,bg='DodgerBlue')
    cd.grid(row=0,column=2,sticky='news')
    
    # Create Entry Frame Entry
    namemed = Entry(entry_frame,justify=CENTER)
    namemed.grid(row=1,column=0,ipady=10,ipadx=69)
    amount = Entry(entry_frame,justify=CENTER)
    amount.grid(row=1,column=1,ipady=10,ipadx=2)
    unit = Entry(entry_frame,justify=CENTER)
    unit.grid(row=1,column=2,ipady=10,ipadx=2)

    mytree_stock = treeview()

# Customer
def customerWindow():

    #ข้อมูลเมนู
    def Menulist_Window(table_num):

        def orderWindow(menu_name,cost):
            
            def confirmOrder(amt):
                # Variable
                nonlocal table_num

                amt = int(amt)
                selectTable = "Table " + str(table_num)
                print(selectTable)

                # Check if amt is 0 Cancel
                if amt != 0:
                    # import menu info from database
                    sql = '''
                            SELECT * FROM Menu Where prodname = ?;
                        '''
                    cursor.execute(sql,[menu_name])
                    selectMenu = cursor.fetchone()

                    if selectMenu == None:
                        sql = '''
                                SELECT * FROM Promotion Where promoname = ?;
                            '''
                        cursor.execute(sql,[menu_name])
                        selectMenu = cursor.fetchone()

                    cost = int(selectMenu[2])*amt
                    print(cost)

                    # save order to table database
                    cursor.execute("INSERT INTO OrderList (tableName, name, amount, price, totalCost) VALUES(?,?,?,?,?);", (selectTable,selectMenu[1],amt, selectMenu[2], cost))
                    conn.commit()

                    # Close order window
                cancelOrder()

            def cancelOrder():
                nonlocal confirmWin
                confirmWin.destroy()
                confirmWin = None
            
            # Not Finish
            def check_input(event):
                # Combobox List
                value = event.widget.get()

                if value == '':
                    disamt_box['values'] = avl_lst
                else:
                    data = []
                    for item in avl_lst:
                        if value.lower() in item.lower():
                            data.append(item)

                    disamt_box['values'] = data
            
            # Variable
            avl_lst = []
            amt = IntVar()
            nonlocal confirmWin
            nonlocal table_num

            # Add Number to avl_lst
            for i in range(1, 51):
                avl_lst.append(i)

            # Check Exist Confirm Window
            if confirmWin != None:
                return None
            

            # Create Confirm Window parent is 'window', value[1] is medical Name
            confirmWin = Toplevel(window)
            confirmWin.geometry("280x100")
            confirmWin.configure(bg='#daf7fa')
            Label(confirmWin, text=menu_name + " "+ cost,font="Tahoma 12 ",bg='#daf7fa').place(x=10,y=0)
            Label(confirmWin, text='Amount of order :',bg='#daf7fa').place(x=10,y=30)
            # Order amount combo box
            disamt_box = ttk.Combobox(confirmWin, textvariable=amt, state='readonly')
            disamt_box['values'] = avl_lst
            disamt_box.bind('<KeyRelease>', check_input)
            disamt_box.place(x=130,y=32)

            # Exit and Confirm 
            exbt = Button(confirmWin, text = 'Cancel', command=cancelOrder)
            exbt.place(x=50,y=60)
            cfbt = Button(confirmWin, text = 'Confirm', command=lambda:confirmOrder(amt.get()))
            cfbt.place(x=170,y=60)

        def Menu_Window():
            MenuCake=Frame(MainFrame, height=1000, width=1000, bg='#735000')
            MenuCake.place(x=0,y=0)
            Label(MenuCake,text='Menu Cake',font='Tohoma 18 bold',fg='white',bg='#735000').place(x=400,y=10)
            Button(MenuCake,image=cake1,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38, command=lambda:orderWindow("Cream puff","70")).place(x=60 ,y=70)  
            Button(MenuCake,image=cake2,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38, command=lambda:orderWindow("Cheese cake","60")).place(x=370 ,y=70) 
            Button(MenuCake,image=cake3,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38, command=lambda:orderWindow("Tiramisu cake","90")).place(x=660 ,y=70) 
            Button(MenuCake,image=cake4,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38, command=lambda:orderWindow("Pudding cake","65")).place(x=60 ,y=300) 
            Button(MenuCake,image=cake5,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38, command=lambda:orderWindow("Cupcake","75")).place(x=370 ,y=300) 
            Button(MenuCake,image=cake6,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38, command=lambda:orderWindow("Muffin","80")).place(x=660 ,y=300) 
            back = Button(MenuCake,text='Back',height=2,width=7,font='Tohoma 10 bold',command=MainFrame.destroy)
            back.place(x=20,y=520)
            next = Button(MenuCake,text='Next',height=2,width=7,font='Tohoma 10 bold',command=Coffee_Window)
            next.place(x=800,y=520)

        def Coffee_Window():
            Menucoffee=Frame(MainFrame, height=1000, width=1000, bg='#735000')
            Menucoffee.place(x=0,y=0)
            Label(Menucoffee,text='Menu Coffee',font='Tohoma 18 bold',fg='white',bg='#735000').place(x=400,y=10)
            Button(Menucoffee,image=coffee1,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38, command=lambda:orderWindow("Espresso","55")).place(x=60 ,y=70) 
            Button(Menucoffee,image=coffee2,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38, command=lambda:orderWindow("Mocca","55")).place(x=370 ,y=70) 
            Button(Menucoffee,image=coffee3,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38, command=lambda:orderWindow("Capuccino","55")).place(x=660 ,y=70) 
            Button(Menucoffee,image=coffee4,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38, command=lambda:orderWindow("Latte","50")).place(x=60 ,y=300) 
            Button(Menucoffee,image=coffee5,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38, command=lambda:orderWindow("Caramel machiato","50")).place(x=370 ,y=300) 
            Button(Menucoffee,image=coffee6,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38, command=lambda:orderWindow("Soda","50")).place(x=660 ,y=300) 
            back = Button(Menucoffee,text='Back',height=2,width=7,font='Tohoma 10 bold',command=Menucoffee.destroy)
            back.place(x=20,y=520)
            next = Button(Menucoffee,text='Next',height=2,width=7,font='Tohoma 10 bold',command=Promotion_Window)
            next.place(x=800,y=520)

        def Promotion_Window():
            MenuPromotion=Frame(MainFrame, height=1000, width=1000, bg='#735000')
            MenuPromotion.place(x=0,y=0)
            Label(MenuPromotion,text='Menu Protion',font='Tohoma 18 bold',fg='white',bg='#735000').place(x=400,y=10)
            Button(MenuPromotion,image=promo1,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38, command=lambda:orderWindow("Set1","120")).place(x=200 ,y=70) 
            Button(MenuPromotion,image=promo2,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38, command=lambda:orderWindow("Set2","99")).place(x=550 ,y=70) 
            Button(MenuPromotion,image=promo3,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38, command=lambda:orderWindow("Set3","99")).place(x=200 ,y=300) 
            Button(MenuPromotion,image=promo4,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38, command=lambda:orderWindow("Set4","99")).place(x=550 ,y=300) 

            back = Button(MenuPromotion,text='Back',height=2,width=7,font='Tohoma 10 bold',command=MenuPromotion.destroy)
            back.place(x=20,y=520)

        confirmWin = None
        MainFrame=Frame(window, height=1000, width=1000, bg='#735000')
        MainFrame.place(x=0,y=0)
        Menu_Window()

    table_num = IntVar()

    f1=Frame(window, height=1000, width=1000, bg='#735000').place(x=0,y=0)
    Label(f1,text='Table',font='Tohoma 18 bold',fg='white',bg='#735000').place(x=400,y=10)
    Table_number = Spinbox(f1, from_= 1, to = 7,width=10,font="Tohoma 20 bold", textvariable=table_num)
    Table_number.place(x=350,y=200)
    lg = Button(f1,text='Done',height=3,width=13,font='Tohoma 10 bold',command=lambda:Menulist_Window(table_num.get()))
    lg.place(x=380,y=300)
    back = Button(f1,text='Back',height=3,width=13,font='Tohoma 10 bold',command=loginWindow)
    back.place(x=10,y=500)

#ดูออเดอร์
def viewOrder_Window():

    # See Import Data from Database
    def datalst():
        sql = 'select * from OrderList'
        cursor.execute(sql)
        result = cursor.fetchall()
    
        # Add and change input type to list 
        for i,data in enumerate(result):
            tableName_lst.append(data[1])
            orderName_lst.append(data[2])
            orderAmt_lst.append(int(data[3]))
            orderPrice_lst.append(float(data[4]))
            orderTotalCost_lst.append(float(data[5]))   

    # Refresh Treeview Data
    def refreshDisplay():
        # Variable
        nonlocal tableNum
        nonlocal totalCost
        nonlocal orderTree
        nonlocal orderName_lst
        nonlocal orderAmt_lst
        nonlocal orderPrice_lst
        nonlocal orderTotalCost_lst

        # Reset Treeview Display
        orderTree.delete(*orderTree.get_children())

        # Reset Total Cost
        totalCost = 0

        # Reinsert Treeview Display
        for i, data in enumerate(tableName_lst):
            if (tableNum.get() == tableName_lst[i]):
                totalCost += orderTotalCost_lst[i]
                print(orderName_lst[i], orderAmt_lst[i], orderPrice_lst[i], orderTotalCost_lst[i])
                orderTree.insert('', 'end', values=(orderName_lst[i], orderAmt_lst[i], orderPrice_lst[i], orderTotalCost_lst[i]))

    # Create Treeview
    def createOrderDisplay():

        # Variable
        nonlocal orderName_lst
        nonlocal orderAmt_lst
        nonlocal orderPrice_lst
        nonlocal orderTotalCost_lst
        nonlocal totalCost
        nonlocal tableNum

        # Create Info Treeframe Frame
        treeframe_info = Frame(f1)
        treeframe_info .place(x=60,y=100)

        # Create Scrollbar
        treebar = Scrollbar(treeframe_info)
        treebar.pack(side=RIGHT,fill=Y)
        mytree_info = ttk.Treeview(treeframe_info,columns=("#1","#2","#3","#4"),height=10,yscrollcommand=treebar.set)
        mytree_info.pack()

        # Config scrollbar on the treeview
        treebar.config(command=mytree_info.yview)

        # Rename Heading
        mytree_info.heading("#0",text="",anchor=W)
        mytree_info.heading("#1",text="Order Name",anchor=CENTER)
        mytree_info.heading("#2",text="Amount",anchor=CENTER)
        mytree_info.heading("#3",text="Price",anchor=CENTER)
        mytree_info.heading("#4",text="Total Cost",anchor=CENTER)

        # Adjust Column Size
        mytree_info.column("#0",width=0,minwidth=0)
        mytree_info.column("#1",anchor=W,width=225,minwidth=225)
        mytree_info.column("#2",anchor=W,width=225,minwidth=225)
        mytree_info.column("#3",anchor=W,width=225,minwidth=225)
        mytree_info.column("#4",anchor=W,width=225,minwidth=225)

        # Reset Treeview
        mytree_info.delete(*mytree_info.get_children()) 

        # Insert Data from list into Treeview
        for i, data in enumerate(tableName_lst):
            if (tableNum.get() == tableName_lst[i]):
                totalCost += orderTotalCost_lst[i]
                print(orderName_lst[i], orderAmt_lst[i], orderPrice_lst[i], orderTotalCost_lst[i])
                mytree_info.insert('', 'end', values=(orderName_lst[i], orderAmt_lst[i], orderPrice_lst[i], orderTotalCost_lst[i]))

        return treeframe_info, mytree_info


    # Check Bill
    def checkBill():
        # delete old info, add info to "Income" table  
        def confirmOrder():
            # Variable
            nonlocal tableNum
            nonlocal totalCost
            nonlocal confirmWin

            curr_date = datetime.datetime.now()
            date = curr_date.strftime("%d")+"/"+curr_date.strftime("%m")+"/"+curr_date.strftime("%Y")
            print(date)

            # Add info to Income table
            cursor.execute("INSERT INTO Income (tableName, balance, day) VALUES(?,?,?)", (tableNum.get(), totalCost, date))

            # Delete info from OrderList table
            cursor.execute("DELETE FROM OrderList WHERE tableName == ?", [tableNum.get()])
            conn.commit()

            # Close order window
            confirmWin.destroy()
            confirmWin = None

        def cancelOrder():
            nonlocal confirmWin
            confirmWin.destroy()
            confirmWin = None
        
        
        # Variable
        nonlocal confirmWin
        nonlocal tableNum
        nonlocal totalCost

        # Check Exist Confirm Window
        if confirmWin != None:
            return None
        
        # Create Confirm Window parent is 'window', value[1] is medical Name
        confirmWin = Toplevel(window)
        confirmWin.geometry("280x100")
        confirmWin.configure(bg='#daf7fa')
        Label(confirmWin, text=tableNum.get(),font="Tahoma 12 ",bg='#daf7fa').place(x=10,y=0)
        Label(confirmWin, text='Total Cost : %.2f'%totalCost,bg='#daf7fa').place(x=10,y=30)

        # Exit and Confirm 
        exbt = Button(confirmWin, text = 'Cancel', command=cancelOrder)
        exbt.place(x=50,y=60)
        cfbt = Button(confirmWin, text = 'Confirm', command=confirmOrder)
        cfbt.place(x=170,y=60)

    # Variable
    tableName_lst = []
    orderName_lst = []
    orderAmt_lst = []
    orderPrice_lst = []
    orderTotalCost_lst = []
    confirmWin = None
    totalCost = 0
    tableNum = StringVar()
    datalst()

    f1=Frame(window, height=1000, width=1000, bg='#735000').place(x=0,y=0)
    # Table List Combo Box (textvariable = tableNum)
    Label(f1,text='Table Number',font='Tohoma 18 bold',fg='white',bg='#735000').place(x=300,y=10)
    
    tablelist = ["Table 1","Table 2","Table 3","Table 4","Table 5","Table 6","Table 7"]
    tablebox = ttk.Combobox(f1,textvariable=tableNum,state='readonly')
    tablebox['values'] = tablelist
    tablebox.set("Table 1")
    tablebox.place(x=500,y=18)

    Button(f1,image=imgsearch,bg="#83c7bd",activebackground = "#abd4ce",bd=0.38, command=refreshDisplay).place(x = 650 ,y = 12)
    # Create Order Display (variable = orderDisplay)

    orderDisplay, orderTree = createOrderDisplay()
    orderDisplay.place(x = 20, y = 100)

    back = Button(f1,text='Back',height=3,width=13,font='Tohoma 10 bold',command=ownerMenu)
    back.place(x=10,y=500)

    checkBillButton = Button(f1,text='Check Bill',height=3,width=13,font='Tohoma 10 bold', command=checkBill)
    checkBillButton.place(x=750,y=500)

def view_menu():
        def Menu_List():
            MenuCake=Frame(MainFrame, height=1000, width=1000, bg='#735000')
            MenuCake.place(x=0,y=0)
            Label(MenuCake,text='Menu Cake',font='Tohoma 18 bold',fg='white',bg='#735000').place(x=400,y=10)
            Button(MenuCake,image=cake1,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38).place(x=60 ,y=70)  
            Button(MenuCake,image=cake2,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38).place(x=370 ,y=70) 
            Button(MenuCake,image=cake3,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38).place(x=660 ,y=70) 
            Button(MenuCake,image=cake4,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38).place(x=60 ,y=300) 
            Button(MenuCake,image=cake5,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38).place(x=370 ,y=300) 
            Button(MenuCake,image=cake6,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38).place(x=660 ,y=300) 
            back = Button(MenuCake,text='Back',height=2,width=7,font='Tohoma 10 bold',command=MainFrame.destroy)
            back.place(x=20,y=520)
            next = Button(MenuCake,text='Next',height=2,width=7,font='Tohoma 10 bold',command=Coffee_list)
            next.place(x=800,y=520)

        def Coffee_list():
            Menucoffee=Frame(MainFrame, height=1000, width=1000, bg='#735000')
            Menucoffee.place(x=0,y=0)
            Label(Menucoffee,text='Menu Coffee',font='Tohoma 18 bold',fg='white',bg='#735000').place(x=400,y=10)
            Button(Menucoffee,image=coffee1,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38).place(x=60 ,y=70) 
            Button(Menucoffee,image=coffee2,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38).place(x=370 ,y=70) 
            Button(Menucoffee,image=coffee3,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38).place(x=660 ,y=70) 
            Button(Menucoffee,image=coffee4,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.3).place(x=60 ,y=300) 
            Button(Menucoffee,image=coffee5,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38).place(x=370 ,y=300) 
            Button(Menucoffee,image=coffee6,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38).place(x=660 ,y=300) 
            back = Button(Menucoffee,text='Back',height=2,width=7,font='Tohoma 10 bold',command=Menucoffee.destroy)
            back.place(x=20,y=520)
            next = Button(Menucoffee,text='Next',height=2,width=7,font='Tohoma 10 bold',command=Promotion_list)
            next.place(x=800,y=520)

        def Promotion_list():
            MenuPromotion=Frame(MainFrame, height=1000, width=1000, bg='#735000')
            MenuPromotion.place(x=0,y=0)
            Label(MenuPromotion,text='Menu Protion',font='Tohoma 18 bold',fg='white',bg='#735000').place(x=400,y=10)
            Button(MenuPromotion,image=promo1,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38).place(x=200 ,y=70) 
            Button(MenuPromotion,image=promo2,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38).place(x=550 ,y=70) 
            Button(MenuPromotion,image=promo3,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38).place(x=200 ,y=300) 
            Button(MenuPromotion,image=promo4,bg="#83c7bd",height=200,width=200,activebackground = "#abd4ce",bd=0.38).place(x=550 ,y=300) 

            back = Button(MenuPromotion,text='Back',height=2,width=7,font='Tohoma 10 bold',command=MenuPromotion.destroy)
            back.place(x=20,y=520) 

        MainFrame=Frame(window, height=1000, width=1000, bg='#735000')
        MainFrame.place(x=0,y=0)
        Menu_List()

def import_Img():
    global img1,imgsearch,cake,coffee,cakeAndcoffee
    global cake1,cake2,cake3,cake4,cake5,cake6
    global coffee1,coffee2,coffee3,coffee4,coffee5,coffee6
    global promo1,promo2,promo3,promo4
    img1 = PhotoImage(file='imgset2/userimg.png').subsample(15)
    imgsearch = PhotoImage(file="imgset2/search.png").subsample(2)
    cake = PhotoImage(file="imgset2/cake.png")
    coffee = PhotoImage(file="imgset2/coffee.png").subsample(10)
    cakeAndcoffee = PhotoImage(file="imgset2/CoffeeAndCake.png").subsample(10)
    cake1 = PhotoImage(file="imgset2/creampuff.png").subsample(3)
    cake2 = PhotoImage(file="imgset2/cheesecake.png").subsample(3)
    cake3 = PhotoImage(file="imgset2/tiramisu.png").subsample(3)
    cake4 = PhotoImage(file="imgset2/pudding.png").subsample(3)
    cake5 = PhotoImage(file="imgset2/cupcake.png")
    cake6 = PhotoImage(file="imgset2/muffin.png").subsample(3)
    coffee1 = PhotoImage(file="imgset2/Espresso.png").subsample(5)
    coffee2 = PhotoImage(file="imgset2/Mocha.png").subsample(5)
    coffee3 = PhotoImage(file="imgset2/capu.png").subsample(5)
    coffee4 = PhotoImage(file="imgset2/latte.png").subsample(5)
    coffee5 = PhotoImage(file="imgset2/caramel.png").subsample(5)
    coffee6 = PhotoImage(file="imgset2/soda.png").subsample(5)
    promo1 = PhotoImage(file="imgset2/set1.png").subsample(4)
    promo2 = PhotoImage(file="imgset2/set2.png").subsample(4)
    promo3 = PhotoImage(file="imgset2/set3.png").subsample(4)
    promo4 = PhotoImage(file="imgset2/set4.png").subsample(4)



createconnection()
window = createWindow()

Table_number = IntVar()
puser = StringVar()
ppassword = StringVar()
mainWindow()

window.mainloop()
cursor.close()
conn.close()
